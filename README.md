# anagram #

An application to find an English phrase provided its md5 hash, its anagram, and a list of English words. 

### Usage ###
```
go get -u bitbucket.org/anphung/anagram

cd anagram
go run anagram.go
```