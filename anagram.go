// This is the driver for searching anagram of an English phrase
// given its anagram, its MD5 hash, and a list of english words.
package main

import (
	"log"

	"bitbucket.org/anphung/anagram/anagram"
)

func main() {
	// Initialize our anagram processor
	ap, err := anagram.NewProcessor("wordlist")
	if err != nil {
		log.Fatalln(err)
	}
	anagram := "poultry outwits ants"
	md5Hash := "4624d200580677270a54ccff86b9610e"
	phrase := ""
	log.Println("Anagram: " + anagram)
	log.Println("MD5 hash: " + md5Hash)

	// Fully parallel.
	// Partition raw data to each routine
	log.Println("Start anagram.PhraseWithRoutines1...")
	phrase = ap.PhraseWithRoutines1(anagram, md5Hash)
	log.Printf("Found phrase: %s\n\n", phrase)

	// Partially parallel.
	// Worker pool: This method in theory should be faster than the below
	// (single thread) method, but practically it is slower because of
	// overhead cost of communication with go routines.
	log.Println("Start anagram.PhraseWithRoutines...")
	phrase = ap.PhraseWithRoutines(anagram, md5Hash)
	log.Printf("Found phrase: %s\n\n", phrase)

	// No parallel.
	// Without rountine
	log.Println("Start anagram.Phrase...")
	phrase = ap.Phrase(anagram, md5Hash)
	log.Printf("Found phrase: %s\n\n", phrase)
}
