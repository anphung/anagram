// Package anagram contains anagram initialization
// and processing.
// Performace:
// PhraseWithRoutines1 > Phrase > PhraseWithRoutines
package anagram

import (
	"crypto/md5"
	"fmt"
	"io/ioutil"
	"runtime"
	"sort"
	"strings"
	"sync"

	"github.com/cznic/mathutil"
)

// Struct Processor contains words list to check fo anagram.
type Processor struct {
	words []string
}

// New initizlizes and return *Processor.
func NewProcessor(wordsFile string) (*Processor, error) {
	// Read words list
	data, err := ioutil.ReadFile(wordsFile)
	if err != nil {
		return nil, err
	}

	words := strings.Split(string(data), "\n")

	// Remove duplicates by using set(map[string]bool)
	s := map[string]bool{}
	for _, w := range words {
		w = strings.TrimSpace(w)
		s[w] = true
	}
	words = []string{}
	for w, _ := range s {
		words = append(words, w)
	}

	a := &Processor{words}

	return a, nil
}

// isAnagram checks if 2 string is anagram of one another.
func isAnagram(first, second string) bool {
	if len(first) != len(second) {
		return false
	}

	for _, c := range first {
		if strings.Count(first, string(c)) !=
			strings.Count(second, string(c)) {
			return false
		}
	}
	return true
}

// containsAnagram checks if first contains anagram of second.
func containsAnagram(first, second string) bool {
	if len(first) < len(second) {
		return false
	}

	for _, c := range second {
		if strings.Count(first, string(c)) <
			strings.Count(second, string(c)) {
			return false
		}
	}
	return true
}

// filterAnagram fillters out word in words that could not be "sub anagram"
// of anagram.
func filterAnagram(anagram string, words []string) []string {
	filtered := sort.StringSlice{}
	// Filter out word could not be a "sub anagram" of phrase
	for _, word := range words {
		// Remove empty word
		if word == "" {
			continue
		}
		if containsAnagram(anagram, word) {
			filtered = append(filtered, word)
		}
	}

	// Sort words to have determinated processing time in order to compare
	// performance between searching methods.
	filtered.Sort() // Remove this line in production
	return filtered
}

// permutationWithMD5Hash searches phrase made from a permutation of word
// from words that has md5 hash equals to md5Hash.
func permutationWithMD5Hash(
	words []string,
	md5Hash string) string {
	// Check case corner
	if len(words) == 0 {
		return ""
	}

	// Generate first permutation.
	// We create new slice so that we won't affect words while permutating.
	sl := make([]string, len(words))
	copy(sl, words)
	perm := sort.StringSlice(sl)
	mathutil.PermutationFirst(perm)

	// Iterate and process each permutation
	for {
		// Build phrase from words
		phrase := strings.Join(perm, " ")
		// Check md5 equivalence
		phraseMD5Hash := fmt.Sprintf("%x", md5.Sum([]byte(phrase)))
		if phraseMD5Hash == md5Hash {
			return phrase
		}
		// Get next permutation
		if !mathutil.PermutationNext(perm) {
			return ""
		}
	}
}

// Phrase searches phrase of anagram which has md5 hash equals to md5Hash.
// Algorithm diagram:
// [.......................Data.to.be.processed..............................]
//                                  |
//                                  V
//                               [worker]
//
func (ap *Processor) Phrase(anagram, md5Hash string) string {
	words := filterAnagram(anagram, ap.words)

	// We assume the phrase to be found is "perfect" which means each words
	// seperated by a single space and does not have head or trailing space.
	// Number of word in phrase:
	n := strings.Count(anagram, " ") + 1
	// Remove " " for checking anagram easier
	anagram = strings.Replace(anagram, " ", "", -1)
	foundWords := []string{}

	// f is a recursive function that iterate through all space of combination
	// of words, and search for phrase that satisfies md5 hash.
	var f func(start int) string
	f = func(start int) string {
		if len(foundWords) == n { // Has enough words for a permutation
			// Build phrase from found words
			currPhrase := strings.Join(foundWords, "")
			// Check if this phrase is anagram of phraseFreq
			if isAnagram(anagram, currPhrase) {
				phrase := permutationWithMD5Hash(foundWords, md5Hash)
				if phrase != "" {
					return phrase
				}
			}
			return ""
		}

		phrase := ""
		// Iterate through each word to build combination
		for i := start; i < len(words); i++ {
			// Choose current word
			foundWords = append(foundWords, words[i])
			// only if adding this word does not break anagram condition
			if containsAnagram(anagram, strings.Join(foundWords, "")) {
				phrase = f(i + 1)
			}
			// Remove current word
			foundWords = foundWords[:len(foundWords)-1]
			if phrase != "" {
				return phrase
			}
		}
		return ""
	}

	// Find anagram
	phrase := f(0)
	return phrase
}

// PhraseWithRoutines searches phrase of anagram which has md5 hash equals
// to md5Hash. This method uses go routine to increase performance by
// parallelism. We use worker pool pattern.
// Algorithm diagram:
// [.......................Data.to.be.processed..............................]
//  |(copy)       |(copy)           |(Copy)                           (Copy)|
//  V             V                 V                                       V
// [worker0]  [worker1]            ...                               [wokrerN]
//
func (ap *Processor) PhraseWithRoutines(anagram, md5Hash string) string {
	words := filterAnagram(anagram, ap.words)

	// We assume the phrase to be found is "perfect" which means each words
	// seperated by a single space and does not have head or trailing space.
	// Number of word in phrase:
	n := strings.Count(anagram, " ") + 1
	// Remove " " for checking anagram easier
	anagram = strings.Replace(anagram, " ", "", -1)
	foundWords := []string{}

	// Number of worker enough to utilize parallelism
	nWorker := runtime.GOMAXPROCS(0)
	if runtime.NumCPU() < nWorker {
		nWorker = runtime.NumCPU()
	}
	jobs := make(chan []string, nWorker*2+2)
	results := make(chan string, nWorker*4+2)
	var wg sync.WaitGroup
	wg.Add(nWorker)
	// Create worker
	worker := func() {
		defer wg.Done()
		for j := range jobs {
			// Check if this phrase is anagram of phraseFreq
			if isAnagram(anagram, strings.Join(j, "")) {
				// Search if any permutation of word has md5 hash md5Hash.
				phrase := permutationWithMD5Hash(j, md5Hash)
				if phrase != "" {
					results <- phrase
				}
			}
		}
	}
	// Setup workers
	for i := 0; i < nWorker; i++ {
		go worker()
	}

	phrase := ""
	// f is a recursive function that iterate through all space of combination
	// of words, and add them to jobs channel.
	var f func(start int)
	f = func(start int) {
		if len(foundWords) == n { // Has enough words for a permutation
			sl := make([]string, len(foundWords))
			copy(sl, foundWords)
			jobs <- sl
			return
		}

		// Iterate through each word to build combination
		for i := start; i < len(words); i++ {
			// Choose current word
			foundWords = append(foundWords, words[i])
			// only if adding this word does not break anagram condition
			if containsAnagram(anagram, strings.Join(foundWords, "")) {
				f(i + 1)
			}
			// Remove current word
			foundWords = foundWords[:len(foundWords)-1]
			// Check if we found the phrase
			if len(results) > 0 {
				return
			}
		}
		return
	}

	// Find anagram
	f(0)
	close(jobs)
	if phrase != "" {
		return phrase
	}
	// Check last time if any worker found phrase
	wg.Wait()
	if len(results) > 0 {
		phrase = <-results
	}
	return phrase
}

// PhraseWithRoutines1 searches phrase of anagram which has md5 hash equals
// to md5Hash. This method uses go routine to increase performance by
// parallelism. It is an improvement from PhraseWithRoutines because it
// use data location (not actual data), so the overhead of communication
// is decreased significantly. We don't use worker pool.
// Algorithm diagram:
// Let Loc0 = Location for partition 0 of data to be processed
// Loc0          Loc1              ...                               LocN
//  |             |                 |                                 |
//  V             V                 V                                 V
// [worker0]  [worker1]            ...                            [wokrerN]
//
func (ap *Processor) PhraseWithRoutines1(anagram, md5Hash string) string {
	words := filterAnagram(anagram, ap.words)

	// We assume the phrase to be found is "perfect" which means each words
	// seperated by a single space and does not have head or trailing space.
	// Number of word in phrase:
	n := strings.Count(anagram, " ") + 1
	// Number of work
	nWork := len(words)
	// Remove " " for checking anagram easier
	anagram = strings.Replace(anagram, " ", "", -1)

	// We only need enough routine to maximize parallemlism.
	nWorker := runtime.GOMAXPROCS(0)
	if runtime.NumCPU() < nWorker {
		nWorker = runtime.NumCPU()
	}
	results := make(chan string, nWorker*2+2)
	worker := func(pos int) {
		foundWords := []string{}
		if nWorker == 0 {
			nWorker = 1
		}
		// Partition the work for each worker in trivial way
		start := pos * (nWork / nWorker)
		end := start + nWork/nWorker
		if pos < (nWork % nWorker) {
			start += pos
			end += pos + 1
		} else {
			start += nWork % nWorker
			end += nWork % nWorker
		}
		// f is a recursive function that iterate through all space of
		// combination of words, and search for phrase that satisfies md5 hash.
		var f func(startPos, endPos int) string
		f = func(startPos, endPos int) string {
			if len(foundWords) == n { // Has enough words for a permutation
				// Build phrase from found words
				currPhrase := strings.Join(foundWords, "")
				// Check if this phrase is anagram of phraseFreq
				if isAnagram(anagram, currPhrase) {
					phrase := permutationWithMD5Hash(foundWords, md5Hash)
					if phrase != "" {
						return phrase
					}
				}
				return ""
			}

			phrase := ""
			// Iterate through each word to build combination
			for i := startPos; i < endPos; i++ {
				// Choose current word
				foundWords = append(foundWords, words[i])
				// only if adding this word does not break anagram condition
				if containsAnagram(anagram, strings.Join(foundWords, "")) {
					phrase = f(i+1, len(words))
				}
				// Remove current word
				foundWords = foundWords[:len(foundWords)-1]
				if phrase != "" {
					return phrase
				}
			}
			return ""
		}

		phrase := f(start, end)
		results <- phrase
	}
	// Start worker
	for i := 0; i < nWorker; i++ {
		go worker(i)
	}
	phrase := ""
	for i := 0; i < nWorker; i++ {
		phrase = <-results
		if phrase != "" {
			break
		}
	}
	return phrase
}
