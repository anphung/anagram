package anagram

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewProcessor(t *testing.T) {
	_, err := NewProcessor("a_test_file")
	assert.Nil(t, err, "Expext nil error")
}

func TestNewProcessorError(t *testing.T) {
	_, err := NewProcessor("aNonExistentFile")
	assert.Error(t, err, "An error was expected")
}

func TestIsAnagram(t *testing.T) {
	var cases = []struct {
		firstIn, secondIn string
		expected          bool
	}{
		// True cases
		{"abc", "cba", true},
		{"cba", "abc", true},
		{"aaab", "abaa", true},
		{"", "", true},
		// False cases
		{"abcde", "abc", false},
		{"abc", "abcde", false},
		{"aaa", "a", false},
		{"a", "aaa", false},
		{"a", "", false},
		{"", "b", false},
		{"aa", "bb", false},
		{"bb", "aa", false},
	}

	for _, c := range cases {
		got := isAnagram(c.firstIn, c.secondIn)
		assert.Equal(t, c.expected, got, fmt.Sprintf("Case: %v", c))
	}
}

func TestContainsAnagram(t *testing.T) {
	var cases = []struct {
		firstIn, secondIn string
		expected          bool
	}{
		// True cases
		{"abc", "ba", true},
		{"abc", "cab", true},
		{"aaa", "a", true},
		{"abc", "", true},
		{"", "", true},
		// False cases
		{"abc", "acf", false},
		{"abc", "aabc", false},
		{"a", "aaa", false},
		{"", "aa", false},
		{"aa", "bb", false},
	}

	for _, c := range cases {
		got := containsAnagram(c.firstIn, c.secondIn)
		assert.Equal(t, c.expected, got, fmt.Sprintf("Case: %v", c))
	}
}

func TestFilterAnagram(t *testing.T) {
	anagram := "abcde"
	words := []string{"abf", "aab", "abc", "abcdef"}
	expected := []string{"abc"}
	got := filterAnagram(anagram, words)
	assert.Equal(t, expected, got, fmt.Sprintf("Case: %v %v %v", anagram, words, expected))
}

func TestPermutationWithMD5Hash(t *testing.T) {
	var cases = []struct {
		words    []string
		md5Hash  string
		expected string
	}{
		{[]string{}, "not a md5 hash", ""},
		{[]string{""}, "not a md5 hash", ""},
		{[]string{"1"}, "not a md5 hash", ""},
		{[]string{"1", "2"}, "not a md5 hash", ""},
		{[]string{""}, "d41d8cd98f00b204e9800998ecf8427e", ""},
		{[]string{"1"}, "c4ca4238a0b923820dcc509a6f75849b", "1"},
		{[]string{"1", "2"}, "31371f199688ae40ba84c37a303411be", "1 2"},
		{[]string{"1", "2"}, "e5f496776d0be90c523ffd4a458d7179", "2 1"},
	}

	for _, c := range cases {
		got := permutationWithMD5Hash(c.words, c.md5Hash)
		assert.Equal(t, c.expected, got, fmt.Sprintf("Case: %v", c))
	}
}

func TestProcessorPhrase(t *testing.T) {
	// Setup anagram processor
	ap := &Processor{}
	ap.words = []string{"1", "2", "12345", "3"}

	var cases = []struct {
		anagram  string
		md5Hash  string
		expected string
	}{
		{"1", "not a md5 hash", ""},
		{"1 2", "not a md5 hash", ""},
		{"1", "c4ca4238a0b923820dcc509a6f75849b", "1"},
		{"1 2", "31371f199688ae40ba84c37a303411be", "1 2"},
		{"1 2", "e5f496776d0be90c523ffd4a458d7179", "2 1"},
		{"3 2", "a2d9b33304a1222854cafaf96187e00d", "2 3"},
		{"12345", "827ccb0eea8a706c4c34a16891f84e7b", "12345"},
	}

	for _, c := range cases {
		got := ap.Phrase(c.anagram, c.md5Hash)
		assert.Equal(t, c.expected, got, fmt.Sprintf("Case: %v", c))
	}

}

func TestProcessorPhraseWithRoutines(t *testing.T) {
	// Setup anagram processor
	ap := &Processor{}
	ap.words = []string{"1", "2", "12345", "3"}

	var cases = []struct {
		anagram  string
		md5Hash  string
		expected string
	}{
		{"1", "not a md5 hash", ""},
		{"1 2", "not a md5 hash", ""},
		{"1", "c4ca4238a0b923820dcc509a6f75849b", "1"},
		{"1 2", "31371f199688ae40ba84c37a303411be", "1 2"},
		{"1 2", "e5f496776d0be90c523ffd4a458d7179", "2 1"},
		{"3 2", "a2d9b33304a1222854cafaf96187e00d", "2 3"},
		{"12345", "827ccb0eea8a706c4c34a16891f84e7b", "12345"},
	}

	for _, c := range cases {
		got := ap.PhraseWithRoutines(c.anagram, c.md5Hash)
		assert.Equal(t, c.expected, got, fmt.Sprintf("Case: %v", c))
	}

}

func TestProcessorPhraseWithRoutines1(t *testing.T) {
	// Setup anagram processor
	ap := &Processor{}
	ap.words = []string{"1", "2", "12345", "3"}

	var cases = []struct {
		anagram  string
		md5Hash  string
		expected string
	}{
		{"1", "not a md5 hash", ""},
		{"1 2", "not a md5 hash", ""},
		{"1", "c4ca4238a0b923820dcc509a6f75849b", "1"},
		{"1 2", "31371f199688ae40ba84c37a303411be", "1 2"},
		{"1 2", "e5f496776d0be90c523ffd4a458d7179", "2 1"},
		{"3 2", "a2d9b33304a1222854cafaf96187e00d", "2 3"},
		{"12345", "827ccb0eea8a706c4c34a16891f84e7b", "12345"},
	}

	for _, c := range cases {
		got := ap.PhraseWithRoutines1(c.anagram, c.md5Hash)
		assert.Equal(t, c.expected, got, fmt.Sprintf("Case: %v", c))
	}

}
